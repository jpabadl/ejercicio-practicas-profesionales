
package lcdtester;

//Interface para declarar los metodos de adicionar que posteriormente se implementaran en la clase Impresor
interface AdicionarInterface {
   public void adicionarLinea(String[][] matriz, int[] punto, String posFija, int tamaño, String caracter);
   public void adicionarSegmento(int segmento);
   public void adicionarDigito(int numero);
   
}