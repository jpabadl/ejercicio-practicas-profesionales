package lcdtester;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ImpresorLCD implements AdicionarInterface {

    // Puntos fijos
    private final int[] pf1;
    private final int[] pf2;
    private final int[] pf3;
    private final int[] pf4;
    private final int[] pf5;
    private String[][] matrizImpr;

    static final String CARACTER_VERTICAL = "|";
    static final String CARACTER_HORIZONTAL = "-";
    static final String POSICION_X = "X";
    static final String POSICION_Y = "Y";

    // TODO code application logic here
    //String entrada = JOptionPane.showInputDialog("Digite el numero");
    private int tamaño;

    // Calcula el numero de filas y columnas por Digito
    private int filasDigito;
    private int columnasDigito;
    private int totalFilas;
    private int totalColumnas;
    
    //Metodos Publicos
    
     /**
     * Devuelve las filas de un digito
     * @return filasDigito
     */
    public int getFilasDigito() {
        return filasDigito;
    }
 
    /**
     * Modifica las filas de un digito
     * @param filasDigito
     */
    public void setFilasDigito(int filasDigito) {
        this.filasDigito = filasDigito;
    }
    
     /**
     * Devuelve las columnas de un digito
     * @return columnasDigito
     */
    public int getColumnasDigito() {
        return columnasDigito;
    }
 
    /**
     * Modifica las columnas de un digito
     * @param columnasDigito
     */
    public void setColumnasDigito(int columnasDigito) {
        this.columnasDigito = columnasDigito;
    }
    
    /**
     * Devuelve el total de filas
     * @return totalFilas
     */
    public int getTotalFilas() {
        return totalFilas;
    }
 
    /**
     * Modifica el total de filas
     * @param totalFilas
     */
    public void setTotalFilas(int totalFilas) {
        this.totalFilas = totalFilas;
    }
    
    /**
     * Devuelve el total de columnas
     * @return totalColumnas
     */
    public int getTotalColumnas() {
        return totalColumnas;
    }
 
    /**
     * Modifica el total de columnas
     * @param totalColumnas
     */
    public void setTotalColumnas(int totalColumnas) {
        this.totalColumnas = totalColumnas;
    }
    
     /**
     * Devuelve el tamaño de un digito
     * @return tamaño
     */
    public int getTamaño() {
        return tamaño;
    }
 
    /**
     * Modifica la tamaño de un digito
     * @param tamaño
     */
    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }
    
    /**
     * Devuelve La constante Y
     * @return POSICION_Y
     */
    
    public String getPOSICION_X() {
        return POSICION_X;
    }
    
    /**
     * Devuelve La constante X
     * @return POSICION_X
     */
    public String getPOSICION_Y() {
        return POSICION_Y;
    }
   

    public ImpresorLCD() {
        // Inicializa variables
        this.pf1 = new int[2];
        this.pf2 = new int[2];
        this.pf3 = new int[2];
        this.pf4 = new int[2];
        this.pf5 = new int[2];
    }

    /**
     *
     * Metodo encargado de añadir una linea a la matriz de Impresion
     *
     * @param matriz Matriz Impresion
     * @param punto Punto Pivote
     * @param posFija Posicion Fija
     * @param tamaño TamaÃ±o Segmento
     * @param caracter Caracter Segmento
     */    
    public void adicionarLinea(String[][] matriz, int[] punto, String posFija,
            int tamaño, String caracter) {

        if (posFija.equalsIgnoreCase(this.getPOSICION_X())) 
        {
            for (int y = 1; y <= tamaño; y++) 
            {
                int valor = punto[1] + y;
                matriz[punto[0]][valor] = caracter;
            }
        } 
        else 
        {
            for (int i = 1; i <= tamaño; i++) 
            {
                int valor = punto[0] + i;
                matriz[valor][punto[1]] = caracter;
            }
        }
    }

    /**
     *
     * Metodo encargado de un segmento a la matriz de Impresion
     *
     * @param segmento Segmento a adicionar
     */  
    public void adicionarSegmento(int segmento) {

        switch (segmento) {
            case 1:
                adicionarLinea(this.matrizImpr, this.pf1, this.getPOSICION_Y(),
                        this.getTamaño(), CARACTER_VERTICAL);
                break;
            case 2:
                adicionarLinea(this.matrizImpr, this.pf2, getPOSICION_Y(),
                        this.getTamaño(), CARACTER_VERTICAL);
                break;
            case 3:
                adicionarLinea(this.matrizImpr, this.pf5, getPOSICION_Y(),
                        this.getTamaño(), CARACTER_VERTICAL);
                break;
            case 4:
                adicionarLinea(this.matrizImpr, this.pf4, getPOSICION_Y(),
                        this.getTamaño(), CARACTER_VERTICAL);
                break;
            case 5:
                adicionarLinea(this.matrizImpr, this.pf1, getPOSICION_X(),
                        this.getTamaño(), CARACTER_HORIZONTAL);
                break;
            case 6:
                adicionarLinea(this.matrizImpr, this.pf2, getPOSICION_X(),
                        this.getTamaño(), CARACTER_HORIZONTAL);
                break;
            case 7:
                adicionarLinea(this.matrizImpr, this.pf3, getPOSICION_X(),
                        this.getTamaño(), CARACTER_HORIZONTAL);
                break;
            default:
                break;
        }
    }

    /**
     *
     * Metodo encargado de definir los segmentos que componen un digito y
     * a partir de los segmentos adicionar la representacion del digito a la matriz
     *
     * @param numero Digito
     */
    public void adicionarDigito(int numero) {

        // Establece los segmentos de cada numero
        List<Integer> segList = new ArrayList<>();

        switch (numero) {
            case 1:
                segList.add(3);
                segList.add(4);
                break;
            case 2:
                segList.add(5);
                segList.add(3);
                segList.add(6);
                segList.add(2);
                segList.add(7);
                break;
            case 3:
                segList.add(5);
                segList.add(3);
                segList.add(6);
                segList.add(4);
                segList.add(7);
                break;
            case 4:
                segList.add(1);
                segList.add(6);
                segList.add(3);
                segList.add(4);
                break;
            case 5:
                segList.add(5);
                segList.add(1);
                segList.add(6);
                segList.add(4);
                segList.add(7);
                break;
            case 6:
                segList.add(5);
                segList.add(1);
                segList.add(6);
                segList.add(2);
                segList.add(7);
                segList.add(4);
                break;
            case 7:
                segList.add(5);
                segList.add(3);
                segList.add(4);
                break;
            case 8:
                segList.add(1);
                segList.add(2);
                segList.add(3);
                segList.add(4);
                segList.add(5);
                segList.add(6);
                segList.add(7);
                break;
            case 9:
                segList.add(1);
                segList.add(3);
                segList.add(4);
                segList.add(5);
                segList.add(6);
                segList.add(7);
                break;
            case 0:
                segList.add(1);
                segList.add(2);
                segList.add(3);
                segList.add(4);
                segList.add(5);
                segList.add(7);
                break;
            default:
                break;
        }

        Iterator<Integer> iterator = segList.iterator();

        while (iterator.hasNext()) {
            adicionarSegmento(iterator.next());
        }
    }

    /**
     *
     * Metodo encargado de imprimir un numero
     *
     * @param tamaño TamaÃ±o Segmento Digitos
     * @param numeroImp Numero a Imprimir
     * @param espacio Espacio Entre digitos
     */    
    private void imprimirNumero(int tamaño, String numeroImp, int espacio) 
    {
        int pivotX = 0;
        char[] digitos;

        this.tamaño = tamaño;

        // Calcula el numero de filas cada digito
        this.filasDigito = (2 * this.tamaño) + 3;

        // Calcula el numero de columna de cada digito
        this.columnasDigito = this.tamaño + 2;

        // Calcula el total de filas de la matriz en la que se almacenaran los digitos
        this.totalFilas = this.filasDigito;

        // Calcula el total de columnas de la matriz en la que se almacenaran los digitos
        this.totalColumnas = (this.columnasDigito * numeroImp.length())
                + (espacio * numeroImp.length());

        // crea matriz para almacenar los numero a imprimir
        this.matrizImpr = new String[this.totalFilas][this.totalColumnas];

        // crea el arreglo de digitos
        digitos = numeroImp.toCharArray();

        // Inicializa matriz
        for (int i = 0; i < this.totalFilas; i++) {
            for (int j = 0; j < this.totalColumnas; j++) {
                this.matrizImpr[i][j] = " ";
            }
        }

        for (char digito : digitos) {
            
            //Valida que el caracter sea un digito
            if( ! Character.isDigit(digito))
            {
                throw new IllegalArgumentException("Caracter " + digito
                    + " no es un digito");
            }

            int numero = Integer.parseInt(String.valueOf(digito));

            //Calcula puntos fijos
            this.pf1[0] = 0;
            this.pf1[1] = 0 + pivotX;

            this.pf2[0] = (this.filasDigito / 2);
            this.pf2[1] = 0 + pivotX;

            this.pf3[0] = (this.filasDigito - 1);
            this.pf3[1] = 0 + pivotX;

            this.pf4[0] = (this.columnasDigito - 1);
            this.pf4[1] = (this.filasDigito / 2) + pivotX;

            this.pf5[0] = 0;
            this.pf5[1] = (this.columnasDigito - 1) + pivotX;

            pivotX = pivotX + this.columnasDigito + espacio;

            adicionarDigito(numero);
        }

        // Imprime matriz
        for (int i = 0; i < this.totalFilas; i++) {
            for (int j = 0; j < this.totalColumnas; j++) {
                System.out.print(this.matrizImpr[i][j]);
            }
            System.out.println();
        }
    }

     /**
     *
     * Metodo encargado de procesar la entrada que contiene el tamaño del segmento
 de los digitos y los digitos a imprimir
     *
     * @param comando Entrada que contiene el tamaño del segmento de los digito
 y el numero a imprimir
     * @param espacioDig Espacio Entre digitos
     */  
    public void procesar(String comando, int espacioDig) {
        
        String[] parametros;
        
        int tam;

        if (!comando.contains(",")) {
            throw new IllegalArgumentException("Cadena " + comando
                    + " no contiene caracter ,");
        }
        
        //Se hace el split de la cadena
        parametros = comando.split(",");
        
        //Valida la cantidad de parametros
        if(parametros.length>2)
        {
           throw new IllegalArgumentException("Cadena " + comando
                    + " contiene mas caracter ,"); 
        }
        
        //Valida la cantidad de parametros
        if(parametros.length<2)
        {
           throw new IllegalArgumentException("Cadena " + comando
                    + " no contiene los parametros requeridos"); 
        }
        
        //Valida que el parametro tamaño sea un numerico
        if(isNumeric(parametros[0]))
        {
            tam = Integer.parseInt(parametros[0]);
            
            // se valida que el tamaño este entre 1 y 10
            if(tam <1 || tam >10)
            {
                throw new IllegalArgumentException("El parametro tamaño ["+tam
                        + "] debe estar entre 1 y 10");
            }
        }
        else
        {
            throw new IllegalArgumentException("Parametro tamaño [" + parametros[0]
                    + "] no es un numero");
        }

        // Realiza la impresion del numero
        imprimirNumero(tam, parametros[1],espacioDig);

    }

    /**
     *
     * Metodo encargado de validar si una cadena es numerica
     *
     * @param cadena Cadena
     */  
    static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

}
